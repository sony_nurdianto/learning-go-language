package main

import "fmt"

func main() {

	var vv string = "some Text"

	fmt.Printf("%v\n", vv)
	fmt.Printf("%#v\n", vv)
	fmt.Printf("%T\n", vv)
	fmt.Printf("%s\n", vv)
	fmt.Printf("%x\n", vv)
	fmt.Printf("%q\n", vv)

}
