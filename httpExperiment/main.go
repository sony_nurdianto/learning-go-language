package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func main() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		fmt.Println(r.URL.Query())
		fmt.Println(r.URL.Query()["name"][0])

		jsonR := map[string]any{
			"a": "apple",
			"b": "banana",
			"c": map[string]string{
				"ca": "comunity",
			},
		}

		c, _ := json.Marshal(jsonR)

		w.Write([]byte(c))
	})

	http.ListenAndServe(":8080", nil)

}
