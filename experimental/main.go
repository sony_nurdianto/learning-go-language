package main

import (
	"encoding/json"
	"experimental/experimentalMapAndArray"
	"fmt"
)

func main() {

	experimentalMapAndArray.ExperimentArray()
	experimentalMapAndArray.ExperimentMap()

	jsonStr := `{"a":"apple", "b":"banana"}`
	x := map[string]string{}
	y := map[string]string{
		"a": "appolo",
		"b": "buffalo",
	}

	s, _ := json.Marshal(&y)

	fmt.Printf("%T\n", string(s))

	json.Unmarshal([]byte(jsonStr), &x)

	fmt.Println(x)

	arr := make([]int, 2, 2)

	arr = append(arr, 3)

	fmt.Println(arr)
	fmt.Println(len(arr))
	fmt.Println(cap(arr))
	fmt.Printf("%T\n", y)

}
