package experimentalMapAndArray

import "fmt"

func ExperimentMap() {
	someMap := map[string]interface{}{
		"name": "sony nurdianto",
		"age":  24,
	}

	someMapString := map[string]string{
		"name": "sony nurdianto",
		"age":  "24",
	}

	var someMap2 map[string]interface{} = map[string]interface{}{
		"name": "sony nurdianto",
		"age":  24,
	}

	var dynamicMap map[string]any = map[string]any{
		"age":  24,
		"name": "someName",
	}
	fmt.Println(dynamicMap)

	fmt.Println(someMap)
	fmt.Println(someMapString)
	fmt.Println(someMap2)

}
