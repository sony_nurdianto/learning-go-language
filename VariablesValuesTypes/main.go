package main

import "fmt"

func main() {

	var somenum int

	somenum = 11

	anothernum := &somenum

	*anothernum = 90

	fmt.Println(somenum)
	fmt.Println(*anothernum)

}
